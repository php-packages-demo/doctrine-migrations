# doctrine/migrations

DBAL tool for versioning your database schema and easily deploying changes to it. https://packagist.org/packages/doctrine/migrations

![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/migrations)

* https://www.doctrine-project.org/projects/migrations

## Unofficial documentation
* [*Symfony/Doctrine migrations for multiple databases*
  ](https://dev.to/rafaelberaldo/symfony-doctrine-migrations-for-multiple-databases-drivers-1a07)
  2023-05 Rafael Beraldo
* [*Mixed DDL and DML SQL Commands in Doctrine Migrations*
  ](https://www.ghanei.net/mixed-ddl-dml-in-doctrine-migrations/)
  2015-10 Farzad Ghanei

## Similar tools
* [robmorgan/phinx](https://phppackages.org/p/robmorgan/phinx)
